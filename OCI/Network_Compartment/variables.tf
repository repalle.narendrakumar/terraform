# Defines compartment name for instance
variable "InstanceCompName" {
  default = "BES_DDI_Compartment"
}

# Defines compartment name for instance
variable "NetworkCompName" {
  default = "Network_Compartment"
}

#VCNName
variable "vcn_name" {
  default = "Dev_VCN"
}

#subnets
variable "BES_DDI_DEV_Subnet_Name" {
  default = "BES_DDI_DEV_Subnet"
}
variable "BES_DDI_SH_DEV_Subnet_Name" {
  default = "BES_DDI_SH_DEV_Subnet"
}
variable "BES_DDI_Build_Subnet_Name" {
  default = "BES_DDI_Build_Subnet"
}


#Pubkey
variable "ssh_public_key" {
  default = "/root/.ssh/id_rsa.pub"
}

variable "BootStrapFile" {
  default = "./userdata/startup.sh"
}

variable "BES_DDI_DEV_Subnet_instances" {
  default = {
    "0" = "dev21cvsbl01|VM.Standard2.2|ocid1.image.oc1.phx.aaaaaaaapxvrtwbpgy3lchk2usn462ekarljwg4zou2acmundxlkzdty4bjq|false|FAULT-DOMAIN-1|100|OEL 7.6"
    "1" = "dev21cvsoa01|VM.Standard2.1|ocid1.image.oc1.phx.aaaaaaaapxvrtwbpgy3lchk2usn462ekarljwg4zou2acmundxlkzdty4bjq|false|FAULT-DOMAIN-2|50|OEL 7.6"
    "2" = "dev21cvprt01|VM.Standard2.1|ocid1.image.oc1.phx.aaaaaaaapxvrtwbpgy3lchk2usn462ekarljwg4zou2acmundxlkzdty4bjq|false|FAULT-DOMAIN-3|50|OEL 7.6"
  }
}

variable "BES_DDI_SH_DEV_Subnet_instances" {
  default = {
    "0" = "dev91cvucm01|VM.Standard2.2|ocid1.image.oc1.phx.aaaaaaaapxvrtwbpgy3lchk2usn462ekarljwg4zou2acmundxlkzdty4bjq|false|FAULT-DOMAIN-1|100|OEL 7.6"
    "1" = "dev91cvedq01|VM.Standard2.1|ocid1.image.oc1.phx.aaaaaaaapxvrtwbpgy3lchk2usn462ekarljwg4zou2acmundxlkzdty4bjq|false|FAULT-DOMAIN-2|50|OEL 7.6"
    "2" = "dev91cvoim01|VM.Standard2.2|ocid1.image.oc1.phx.aaaaaaaapxvrtwbpgy3lchk2usn462ekarljwg4zou2acmundxlkzdty4bjq|false|FAULT-DOMAIN-1|150|OEL 7.6"
    "3" = "dev91cvoam01|VM.Standard2.2|ocid1.image.oc1.phx.aaaaaaaapxvrtwbpgy3lchk2usn462ekarljwg4zou2acmundxlkzdty4bjq|false|FAULT-DOMAIN-1|100|OEL 7.6"
    "4" = "dev91cvods01|VM.Standard2.1|ocid1.image.oc1.phx.aaaaaaaapxvrtwbpgy3lchk2usn462ekarljwg4zou2acmundxlkzdty4bjq|false|FAULT-DOMAIN-1|50|OEL 7.6"
    "5" = "dev91cvopa01|VM.Standard2.1|ocid1.image.oc1.phx.aaaaaaaapxvrtwbpgy3lchk2usn462ekarljwg4zou2acmundxlkzdty4bjq|false|FAULT-DOMAIN-1|50|OEL 7.6"
    "6" = "dev91cvsoa01|VM.Standard2.1|ocid1.image.oc1.phx.aaaaaaaapxvrtwbpgy3lchk2usn462ekarljwg4zou2acmundxlkzdty4bjq|false|FAULT-DOMAIN-1|50|OEL 7.6"
    "7" = "dev91cvoes01|VM.Standard2.1|ocid1.image.oc1.phx.aaaaaaaapxvrtwbpgy3lchk2usn462ekarljwg4zou2acmundxlkzdty4bjq|false|FAULT-DOMAIN-1|50|OEL 7.6"
    "8" = "dev91cvecm01|VM.Standard2.2|ocid1.image.oc1.phx.aaaaaaaadg4pvnxn475fb6snyxagqzbgvuswsyfsdrw4cfpedy2vz2yptyzq|false|FAULT-DOMAIN-3|256|Windows 2012 R2 6.3"
    "9" = "dev91cvdcap01|VM.Standard2.1|ocid1.image.oc1.phx.aaaaaaaadg4pvnxn475fb6snyxagqzbgvuswsyfsdrw4cfpedy2vz2yptyzq|false|FAULT-DOMAIN-3|256|Windows 2012 R2 6.3"
  }
}

variable "BES_DDI_Build_Subnet_instances" {
  default = {
   "0" = "build01c|VM.Standard2.2|ocid1.image.oc1.phx.aaaaaaaadg4pvnxn475fb6snyxagqzbgvuswsyfsdrw4cfpedy2vz2yptyzq|false|FAULT-DOMAIN-3|256|Windows 2012 R2 6.3"
  }
}

locals {
  OperatingSystemVersion = "OEL 7.6"
}