resource "oci_core_instance" "instances" {
  count               = "${var.NumInstances}"
  availability_domain = "${data.oci_identity_availability_domain.ad.name}"
  compartment_id      = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
  display_name        = "${element(var.instance_names, count.index)}"
  shape               = "${var.instance_shape}"

  create_vnic_details {
    subnet_id = "${data.oci_core_subnets.my_pub_subnet.subnets.0.id}"
    assign_public_ip = "${var.assign_public_ip}"
    display_name = "public_nic"
  }

  source_details {
    source_type = "image"
    source_id   = "${var.instance_image_ocid[var.region]}"
  }

  metadata {
    ssh_authorized_keys = "${file(var.ssh_public_key)}"
    user_data           = "${base64encode(file(var.BootStrapFile))}"
  }
}
