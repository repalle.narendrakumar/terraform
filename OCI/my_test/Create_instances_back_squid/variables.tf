#Region
variable "region" {
  default = "eu-frankfurt-1"
}

# Defines compartment name for instance
variable "compartment_name" {
  default = "networkcompartment"
}

# Defines the number of instances to deploy
variable "NumInstances" {
  default = "1"
}

#Name
variable "instance_names" {
  type    = "list"
  default = ["squid_server"]
}

#shape
variable "instance_shape" {
  default = "VM.Standard2.1"
}

#Public subnetName
variable "pub_subnet_name" {
  default = "amipubsub1"
}

#Private subnetName
variable "pri_subnet_name" {
  default = "amiprisub1"
}

#VCNName
variable "vcn_name" {
  default = "amidevvcn1"
}

#PublicIP
variable "assign_public_ip" {
  default = "true"
}

#Pubkey
variable "ssh_public_key" {
  default = "/root/.ssh/id_rsa.pub"
}

variable "instance_image_ocid" {
  type = "map"

  default = {
    // See https://docs.us-phoenix-1.oraclecloud.com/images/
    // Oracle-provided image "Centos7"
    us-ashburn-1 = "ocid1.image.oc1.iad.aaaaaaaatsqxhwfybqdv3knwtyacy27rgjuceb7756fcvmvxb3c5h6fvlk4a"

    eu-frankfurt-1 = "ocid1.image.oc1.eu-frankfurt-1.aaaaaaaan7ghs3nhu6bbujqnyukj755642xnmzshck5pm5svol6uigkxl2hq"
    us-phoenix-1 = "ocid1.image.oc1.phx.aaaaaaaaa2ph5vy4u7vktmf3c6zemhlncxkomvay2afrbw5vouptfbydwmtq"
  }
}

variable "BootStrapFile" {
  default = "./userdata/userscript.sh"
}

variable "my_sec_list_name" {
  default = "my_sec_list"
}