resource "oci_core_vnic_attachment" "pri_sub_attach" {
    #Required
    create_vnic_details {
        #Required
        subnet_id = "${data.oci_core_subnets.my_pri_subnet.subnets.0.id}"
        assign_public_ip = false
    }
    instance_id = "${oci_core_instance.instances.id}"
    display_name = "private_nic"
}