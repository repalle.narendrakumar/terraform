data "oci_identity_compartments" "mycomp" {
  compartment_id = "${var.tenancy_ocid}"
  filter {
    name   = "name"
    values = ["${var.compartment_name}"]
  }
  }
data "oci_core_vcns" "myvcn" {
  #Required
  compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
  filter {
    name   = "display_name"
    values = ["${var.vcn_name}"]
  }
}

data "oci_core_subnets" "mysubnets" {
  #Required
  compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
  vcn_id         = "${data.oci_core_vcns.myvcn.virtual_networks.0.id}"
  filter {
    name   = "state"
    values = ["AVAILABLE"]
  }
}

data "oci_core_security_lists" "my_security_list" {
    #Required
    compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
    vcn_id = "${data.oci_core_vcns.myvcn.virtual_networks.0.id}"
         filter {
    name   = "display_name"
    values = ["${var.my_sec_list_name}"]
  } 
}

data "oci_core_route_tables" "my_default_route_table" {
    #Required
    compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
    vcn_id = "${data.oci_core_vcns.myvcn.virtual_networks.0.id}"
    filter {
    name = "display_name"
    values = ["^Default"]
    regex = true
    }
}