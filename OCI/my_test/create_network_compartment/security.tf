resource "oci_core_security_list" "mysecuritylist" {
  compartment_id = "${oci_identity_compartment.compartment.id}"
  vcn_id         = "${oci_core_virtual_network.vnc.id}"
  display_name   = "${var.my_sec_list_name}"

  // allow outbound tcp traffic on all ports
  egress_security_rules {
    destination = "${var.security_list_egress_security_rules_destination}"
    protocol    = "6"
  }

  // allow outbound udp traffic on a all range
  egress_security_rules {
    destination = "${var.security_list_egress_security_rules_destination}"
    protocol    = "17"
  }

    // allow outbound icmp traffic on a all range
  egress_security_rules {
    destination = "${var.security_list_egress_security_rules_destination}"
    protocol    = "1"
  }

  // allow inbound tcp traffic from all port
  ingress_security_rules {
    protocol  = "6"         // tcp
    source    = "${var.security_list_ingress_security_rules_destination}"
  }

   // allow inbound udp traffic from all port
  ingress_security_rules {
    protocol  = "17"         // UDP
    source    = "${var.security_list_ingress_security_rules_destination}"
  }

  // allow inbound icmp traffic of a all type
  ingress_security_rules {
    protocol  = 1
    source    = "${var.security_list_ingress_security_rules_destination}"
  }
}
