// Public subnet
resource "oci_core_subnet" "public_subnet" {
  count             = "${length(var.public_subnet_cidr)}"
  cidr_block        = "${element(var.public_subnet_cidr, count.index)}"
  display_name      = "${element(var.public_subnet_names, count.index)}"
  dns_label         = "${element(var.public_subnet_names, count.index)}"
  compartment_id    = "${oci_identity_compartment.compartment.id}"
  vcn_id            = "${oci_core_virtual_network.vnc.id}"
  security_list_ids = ["${data.oci_core_security_lists.my_security_list.security_lists.0.id}"]
  route_table_id    = "${oci_core_route_table.route_table_internet_gateway.id}"
  dhcp_options_id   = "${oci_core_virtual_network.vnc.default_dhcp_options_id}"
}

// Private subnet
resource "oci_core_subnet" "private_subnet" {
  count                      = "${length(var.private_subnet_cidr)}"
  cidr_block                 = "${element(var.private_subnet_cidr, count.index)}"
  display_name               = "${element(var.private_subnet_names, count.index)}"
  dns_label                  = "${element(var.private_subnet_names, count.index)}"
  compartment_id             = "${oci_identity_compartment.compartment.id}"
  vcn_id                     = "${oci_core_virtual_network.vnc.id}"
  security_list_ids          = ["${data.oci_core_security_lists.my_security_list.security_lists.0.id}"]
  route_table_id             = "${data.oci_core_route_tables.my_default_route_table.route_tables.0.id}"
  prohibit_public_ip_on_vnic = "true"
  dhcp_options_id            = "${oci_core_virtual_network.vnc.default_dhcp_options_id}"
}
