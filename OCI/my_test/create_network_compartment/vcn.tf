// VCN
resource "oci_core_virtual_network" "vnc" {
  cidr_block     = "${var.cidr_block_vnc}"
  compartment_id = "${oci_identity_compartment.compartment.id}"
  display_name   = "${var.vcn_name}"
  dns_label      = "${var.vcn_name}"
}
