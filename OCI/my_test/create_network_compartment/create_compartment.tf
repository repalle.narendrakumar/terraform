resource "oci_identity_compartment" "compartment" {
  name           = "${var.compartment_name}"
  description    = "compartment created by terraform"
  compartment_id = "${var.tenancy_ocid}"
  enable_delete  = "${var.enable_delete_compartment}" // true will cause this compartment to be deleted when running
}
