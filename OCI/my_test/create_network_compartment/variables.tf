//Region
variable "region" {
  default = "us-phoenix-1"
}

//compartment_name
variable "compartment_name" {
  default = "networkcompartment1"
}

variable "enable_delete_compartment" {
  default = "true"
}

//VCN
variable "vcn_name" {
  default = "amidevvcn1"
}

variable "cidr_block_vnc" {
  default = "10.0.0.0/16"
}

//Subnet
variable "public_subnet_cidr" {
  type    = "list"
  default = ["10.0.1.0/24"]
}

variable "private_subnet_cidr" {
  type    = "list"
  default = ["10.0.2.0/24", "10.0.3.0/24"]
}

variable "public_subnet_names" {
  type    = "list"
  default = ["amipubsub1"]
}

variable "private_subnet_names" {
  type    = "list"
  default = ["amiprisub1", "amiprisub2"]
}

//Internet gateway
variable "IG_name" {
  default = "amidev1IG"
}

//NAT Gateway
variable "NAT_name" {
  default = "amidev1NATG"
}

//Route tables
variable "Internet_route_table_name" {
  default = "route_table_internet_gateway"
}
//Egress Security list destination 
variable "security_list_egress_security_rules_destination" {
  default = "0.0.0.0/0"
}

//Ingress Security list destination 
variable "security_list_ingress_security_rules_destination" {
  default = "0.0.0.0/0"
}

variable "my_sec_list_name" {
  default = "my_sec_list"
}

variable "plans" {
  type = "map"

  default = {
    "5USD"  = "1xCPU-1GB"
    "10USD" = "1xCPU-2GB"
    "20USD" = "2xCPU-4GB"
  }
}
