//Internet Gateway
resource "oci_core_internet_gateway" "Internet_gateway" {
  compartment_id = "${oci_identity_compartment.compartment.id}"
  display_name   = "${var.IG_name}"
  vcn_id         = "${oci_core_virtual_network.vnc.id}"
}

//NAT 
resource "oci_core_nat_gateway" "nat_gateway" {
  compartment_id = "${oci_identity_compartment.compartment.id}"
  display_name   = "${var.NAT_name}"
  vcn_id         = "${oci_core_virtual_network.vnc.id}"
}

// Route tables for Internet Gateway
resource "oci_core_route_table" "route_table_internet_gateway" {
  compartment_id = "${oci_identity_compartment.compartment.id}"
  display_name   = "${var.Internet_route_table_name}"

  route_rules {
    network_entity_id = "${oci_core_internet_gateway.Internet_gateway.id}"
    destination       = "0.0.0.0/0"
  }
    route_rules {
    network_entity_id = "${oci_core_drg.drg.id}"
    destination       = "10.20.0.0/16"
  }

  vcn_id = "${oci_core_virtual_network.vnc.id}"
}

// Route tables for NAT Gateway
resource "oci_core_route_table" "route_table_NAT_gateway" {
  compartment_id = "${oci_identity_compartment.compartment.id}"
  display_name   = "${var.NAT_name}"

  route_rules {
    network_entity_id = "${oci_core_nat_gateway.nat_gateway.id}"
    destination       = "0.0.0.0/0"
  }

  vcn_id = "${oci_core_virtual_network.vnc.id}"
}
