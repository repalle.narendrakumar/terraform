
//Region
variable "region" {
  default = "us-phoenix-1"
}

//compartment_name
variable "compartment_name" {
  default = "networkcompartment1"
}

variable "enable_delete_compartment" {
  default = "true"
}

//VCN
variable "vcn_name" {
  default = "amidevvcn1"
}

resource "oci_identity_compartment" "compartment" {
  name           = "${var.compartment_name}"
  description    = "compartment created by terraform"
  compartment_id = "${var.tenancy_ocid}"
  enable_delete  = "${var.enable_delete_compartment}" // true will cause this compartment to be deleted when running
}

// VCN
resource "oci_core_virtual_network" "vnc" {
  cidr_block     = "${var.cidr_block_vnc}"
  compartment_id = "${oci_identity_compartment.compartment.id}"
  display_name   = "${var.vcn_name}"
  dns_label      = "${var.vcn_name}"
}


data "oci_identity_compartments" "mycomp" {
  compartment_id = "${var.tenancy_ocid}"
  filter {
    name   = "name"
    values = ["${var.compartment_name}"]
  }
  }
data "oci_core_vcns" "myvcn" {
  #Required
  compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
  filter {
    name   = "display_name"
    values = ["${var.vcn_name}"]
  }
}