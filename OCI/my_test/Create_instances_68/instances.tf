resource "oci_core_instance" "instances" {
  count               = "${length(var.instance_names)}"
  availability_domain = "${lookup(data.oci_identity_availability_domains.ad.availability_domains[count.index + 1], "name")}"
  compartment_id      = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
  display_name        = "${element(var.instance_names, count.index)}"
  shape               = "${var.instance_shape}"
  #region              = "phx"

  create_vnic_details {
    subnet_id = "${data.oci_core_subnets.my_subnet.subnets.0.id}"
    assign_public_ip = "${var.assign_public_ip}"
    display_name = "${var.my_sub_display_name}"
  }

  source_details {
    source_type = "image"
    source_id   = "${var.instance_image_ocid[var.my_region]}"
  }

  metadata {
    ssh_authorized_keys = "${file(var.ssh_public_key)}"
    user_data           = "${base64encode(file(var.BootStrapFile))}"
  }
}
