#Region
variable "my_region" {
  default = "us-phoenix-1"
}

# Defines compartment name for instance
variable "compartment_name" {
  default = "networkcompartment"
}

#Name
variable "instance_names" {
  type    = "list"
  default = ["instance1"]
}

#shape
variable "instance_shape" {
  default = "VM.Standard2.1"
}

#subnetName
variable "my_subname" {
  default = "amiprisub1"
}

#subnetDisplayName
variable "my_sub_display_name" {
  default = "private_sub"
}


#VCNName
variable "vcn_name" {
  default = "amidevvcn1"
}

#PublicIP
variable "assign_public_ip" {
  default = "false"
}

#Pubkey
variable "ssh_public_key" {
  default = "/root/.ssh/id_rsa.pub"
}

variable "instance_image_ocid" {
  type = "map"

  default = {
    // See https://docs.us-phoenix-1.oraclecloud.com/images/
    // Oracle-provided image "OracleLinux6.8"
    us-phoenix-1 = "ocid1.image.oc1.phx.aaaaaaaawrkdsfnl327clq3cszhhjujxl5rh6htn2arcz5icdih36ohyip2q"
  }
}

variable "BootStrapFile" {
  default = "./userdata/userscript.sh"
}

variable "my_sec_list_name" {
  default = "my_sec_list"
}