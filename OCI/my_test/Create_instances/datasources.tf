data "oci_identity_availability_domains" "ad" {
  compartment_id = "${var.tenancy_ocid}"
}

data "oci_identity_compartments" "mycomp" {
  compartment_id = "${var.tenancy_ocid}"
  filter {
    name   = "name"
    values = ["${var.compartment_name}"]
  }
  }
data "oci_core_vcns" "myvcn" {
  #Required
  compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
  filter {
    name   = "display_name"
    values = ["${var.vcn_name}"]
  }
}

data "oci_core_subnets" "my_subnet" {
  #Required
  compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
  vcn_id         = "${data.oci_core_vcns.myvcn.virtual_networks.0.id}"
  filter {
    name   = "display_name"
    values = ["${var.my_subname}"]
  }
}

data "oci_core_instances" "my_instance" {
    #Required
    compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
      filter {
    name   = "display_name"
    values = ["${var.instance_names}"]
  }
}

data "oci_core_security_lists" "my_security_list" {
    #Required
    compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
    vcn_id = "${data.oci_core_vcns.myvcn.virtual_networks.0.id}"
         filter {
    name   = "display_name"
    values = ["${var.my_sec_list_name}"]
  } 
}