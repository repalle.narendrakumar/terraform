# Output the result
output "show-mycomp" {
  value = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
}

#Output the result
output "show-myvcn" {
  value = "${data.oci_core_vcns.myvcn.virtual_networks.0.id}"
}

# Output the result
#output "show_my_instance_details" {
#  value = "${data.oci_core_instances.my_instance.instances}"
#}

output "InstancePublicIPs" {
  value = ["${oci_core_instance.instances.*.public_ip}"]
}

output "InstancePrivateIPs" {
  value = ["${oci_core_instance.instances.*.private_ip}"]
}


output "list_sec_list" {
  value = "${data.oci_core_security_lists.my_security_list.security_lists.0.id}"
}