# Defines compartment name for instance
variable "InstanceCompName" {
  default = "Management_Servers_Compartment"
}

# Defines compartment name for instance
variable "NetworkCompName" {
  default = "Network_Compartment"
}

#VCNName
variable "vcn_name" {
  default = "Dev_VCN"
}

#subnetName
variable "subnet_name" {
  default = "Management_Public_subnet"
}

#Pubkey
variable "ssh_public_key" {
  default = "/root/.ssh/id_rsa.pub"
}

variable "BootStrapFile" {
  default = "./userdata/startup.sh"
}

variable "instances" {
  default = {
  #"0" = "InstanceName|Shape|imageid|AssignPublicIP|Faultdomain"
    "0" = "AutomationServer|VM.Standard2.2|ocid1.image.oc1.phx.aaaaaaaaa2ph5vy4u7vktmf3c6zemhlncxkomvay2afrbw5vouptfbydwmtq|false|FAULT-DOMAIN-1"
    "1" = "Sourcec|VM.Standard2.1|ocid1.image.oc1.phx.aaaaaaaaa2ph5vy4u7vktmf3c6zemhlncxkomvay2afrbw5vouptfbydwmtq|true|FAULT-DOMAIN-2"
    "2" = "ADC|VM.Standard2.2|ocid1.image.oc1.phx.aaaaaaaadg4pvnxn475fb6snyxagqzbgvuswsyfsdrw4cfpedy2vz2yptyzq|false|FAULT-DOMAIN-1"
    "3" = "PDC|VM.Standard2.2|ocid1.image.oc1.phx.aaaaaaaadg4pvnxn475fb6snyxagqzbgvuswsyfsdrw4cfpedy2vz2yptyzq|false|FAULT-DOMAIN-2"
    "4" = "OpenVPN|VM.Standard2.2|ocid1.image.oc1.phx.aaaaaaaaa2ph5vy4u7vktmf3c6zemhlncxkomvay2afrbw5vouptfbydwmtq|true|FAULT-DOMAIN-1"
  }
}

variable "AddExtraNIC" {
  default = {
  #Num = InstanceID|SubID
  "0" = "ocid1.instance.oc1.phx.abyhqljt3gjg6dctymoswqxtead5nfqhyhhya3cliyonjndsrw5vf4rfuivq|ocid1.subnet.oc1.phx.aaaaaaaazu5sdpschw6vxn3szsaqkmxxo47vqr45lwfygynzng5lu4vfhlza|OpenVPNPrivate|10.40.2.100"
  }
}

/*
    "1" = "ADC|VM.Standard2.2|ocid1.image.oc1.phx.aaaaaaaaum7homajeqcxddkt4thod6w5cfv46ffmjqf7ctquqtpgbomtwsqa|false"
    "2" = "PDC|VM.Standard2.2|ocid1.image.oc1.phx.aaaaaaaaum7homajeqcxddkt4thod6w5cfv46ffmjqf7ctquqtpgbomtwsqa|false"
*/

variable "FileStorageName" {
type = "list"
default = ["HawaiiShare", "Backup"]
}
variable "MountTargetName" {
default = "MountTarget1"
}
variable "export_path" {
type = "list"
default = ["/MountTarget1/share1"]
}
