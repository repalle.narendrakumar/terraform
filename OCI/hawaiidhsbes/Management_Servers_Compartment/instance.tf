resource "oci_core_instance" "instances" {
  count               = "${length(var.instances)}"
  availability_domain = "${data.oci_identity_availability_domains.ad.availability_domains.0.name}"
  compartment_id      = "${data.oci_identity_compartments.InstanceComp.compartments.0.id}"
  display_name        = "${element(split("|",var.instances[count.index]),0)}"
  shape               = "${element(split("|",var.instances[count.index]),1)}"
  fault_domain        = "${element(split("|",var.instances[count.index]),4)}"

  create_vnic_details {
    subnet_id = "${data.oci_core_subnets.my_subnet.subnets.0.id}"
    assign_public_ip = "${element(split("|",var.instances[count.index]),3)}"
    display_name = "${element(split("|",var.instances[count.index]),0)}"
  }

  source_details {
    source_type = "image"
    source_id   = "${element(split("|",var.instances[count.index]),2)}"
  }

  metadata {
    ssh_authorized_keys = "${file(var.ssh_public_key)}"
    user_data           = "${base64encode(file(var.BootStrapFile))}"
  }
}