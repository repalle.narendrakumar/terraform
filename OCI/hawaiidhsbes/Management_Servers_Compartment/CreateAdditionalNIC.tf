resource "oci_core_vnic_attachment" "additionalNIC" {
    count = "${length(var.AddExtraNIC)}"
    #Required
    create_vnic_details {
        #Required
        subnet_id = "${element(split("|",var.AddExtraNIC[count.index]),1)}"
        assign_public_ip = false
        skip_source_dest_check = true
        private_ip = "${element(split("|",var.AddExtraNIC[count.index]),3)}"
    }
    instance_id = "${element(split("|",var.AddExtraNIC[count.index]),0)}"
    display_name = "${element(split("|",var.AddExtraNIC[count.index]),2)}"
}