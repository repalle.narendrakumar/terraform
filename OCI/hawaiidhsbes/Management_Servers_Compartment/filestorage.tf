resource "oci_file_storage_file_system" "file_storage_1" {
  #Required
  count               = "${length(var.FileStorageName)}"
  availability_domain = "${data.oci_identity_availability_domains.ad.availability_domains.0.name}"
  compartment_id      = "${data.oci_identity_compartments.InstanceComp.compartments.0.id}"

  #Optional
  display_name = "${var.FileStorageName[count.index]}"
}

resource "oci_file_storage_mount_target" "mount_target_1" {
  #Required
  availability_domain = "${data.oci_identity_availability_domains.ad.availability_domains.0.name}"
  compartment_id      = "${data.oci_identity_compartments.InstanceComp.compartments.0.id}"
  subnet_id           = "${data.oci_core_subnets.my_subnet.subnets.0.id}"

  #Optional
  display_name = "${var.MountTargetName}"
}

resource "oci_file_storage_export_set" "export_set" {
  # Required
  mount_target_id = "${oci_file_storage_mount_target.mount_target_1.id}"
  display_name = "${var.MountTargetName}"
}

resource "oci_file_storage_export" "file_storage_exports" {
  #Required
  count          = "${length(data.oci_file_storage_file_systems.file_systems.file_systems)}"
  export_set_id  = "${oci_file_storage_export_set.export_set.id}"
  file_system_id = "${lookup(data.oci_file_storage_file_systems.file_systems.file_systems[count.index],"id")}"
  path           = "/${lookup(data.oci_file_storage_file_systems.file_systems.file_systems[count.index],"display_name")}"
    export_options = [
    {
      source                         = "0.0.0.0/0"
      access                         = "READ_WRITE"
      identity_squash                = "NONE"
      require_privileged_source_port = true
    }
    ]
}
