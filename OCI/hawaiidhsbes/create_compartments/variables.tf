variable "compartments" {
  default = {
    "0" = "Network_Compartment,Compartment for network,false"
    "1" = "Management_Servers_Compartment,Compartment for Instance in management servers,false"
    "2" = "Database_Compartment,Compartment for Database,false"
    "3" = "BES_DDI_Compartment,Compartment for Instance in  BES DDI,false"
  }
}
