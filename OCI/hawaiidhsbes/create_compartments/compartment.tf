resource "oci_identity_compartment" "compartment" {
  count          = "${length(var.compartments)}"
  name           = "${element(split(",",var.compartments[count.index]),0)}"
  description    = "${element(split(",",var.compartments[count.index]),1)}"
  compartment_id = "${var.tenancy_ocid}"
  enable_delete  = "${element(split(",",var.compartments[count.index]),2)}"
}