# Defines compartment name for instance
variable "DBCompName" {
  default = "Database_Compartment"
}

# Defines compartment name for instance
variable "NetworkCompName" {
  default = "Network_Compartment"
}

#VCNName
variable "vcn_name" {
  default = "Dev_VCN"
}

#subnetName
variable "db_subnet_name" {
  default = "Database_Client_Subnet"
}

#subnetName
variable "db_backup_subnet_name" {
  default = "Database_Backup_Subnet"
}

#Pubkey
variable "ssh_public_key" {
  default = "/root/.ssh/id_rsa.pub"
}


# DBSystem specific 
variable "db_system_shape" {
  default = "Exadata.Quarter2.92"
}

variable "cpu_core_count" {
  default = "0"
}

variable "db_edition" {
  default = "ENTERPRISE_EDITION_EXTREME_PERFORMANCE"
}

variable "db_admin_password" {
  default = "B35trO0ng_#12"
}

variable "db_name" {
  default = "ExData1"
}

variable "db_version" {
  default = "11.2.0.4"
}

variable "db_home_display_name" {
  default = "Exdb1"
}

variable "db_disk_redundancy" {
  default = "HIGH"
}

variable "sparse_diskgroup" {
  default = true
}

variable "db_system_display_name" {
  default = "Exdb1"
}

variable "hostname" {
  default = "DB11G01"
}

variable "host_user_name" {
  default = "opc"
}

variable "n_character_set" {
  default = "AL16UTF16"
}

variable "character_set" {
  default = "AL32UTF8"
}

variable "db_workload" {
  default = "OLTP"
}

variable "license_model" {
  default = "LICENSE_INCLUDED"
}

variable "node_count" {
  default = "2"
}

variable "time_zone" {
  default = "UTC"
}
