resource "oci_database_db_system" "db_system" {
  availability_domain = "${data.oci_identity_availability_domains.ad.availability_domains.0.name}"
  compartment_id      = "${data.oci_identity_compartments.DBComp.compartments.0.id}"
  cpu_core_count      = "${var.cpu_core_count}"
  database_edition    = "${var.db_edition}"
  time_zone           = "${var.time_zone}"
  lifecycle {
        ignore_changes = ["db_home.0.database.0.admin_password"]
    }
  db_home {
    database {
      admin_password = "${var.db_admin_password}"
      db_name        = "${var.db_name}"
      character_set  = "${var.character_set}"
      ncharacter_set = "${var.n_character_set}"
      db_workload    = "${var.db_workload}"
      #pdb_name       = "${var.pdb_name}"

      db_backup_config {
        auto_backup_enabled = false
      }
    }

    db_version   = "${var.db_version}"
    display_name = "${var.db_home_display_name}"
  }

  disk_redundancy  = "${var.db_disk_redundancy}"
  shape            = "${var.db_system_shape}"
  subnet_id        = "${data.oci_core_subnets.db_subnet.subnets.0.id}"
  backup_subnet_id = "${data.oci_core_subnets.db_backup_subnet.subnets.0.id}"
  ssh_public_keys  = ["${file(var.ssh_public_key)}"]
  display_name     = "${var.db_system_display_name}"
  #sparse_diskgroup = "${var.sparse_diskgroup}"

  hostname                = "${var.hostname}"
  #data_storage_percentage = "${var.data_storage_percentage}"
  license_model = "${var.license_model}"
}



resource "oci_database_exadata_iorm_config" "exadata_iorm_config" {
  db_system_id = "${oci_database_db_system.db_system.id}"
  objective    = "AUTO"

  db_plans = {
    db_name = "default"
    share   = 1
  }
}