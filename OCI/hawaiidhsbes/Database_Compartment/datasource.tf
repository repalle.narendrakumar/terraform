data "oci_identity_availability_domains" "ad" {
  compartment_id = "${var.tenancy_ocid}"
}

data "oci_identity_compartments" "DBComp" {
  compartment_id = "${var.tenancy_ocid}"
  filter {
    name   = "name"
    values = ["${var.DBCompName}"]
  }
  }

data "oci_identity_compartments" "NetworkComp" {
  compartment_id = "${var.tenancy_ocid}"
  filter {
    name   = "name"
    values = ["${var.NetworkCompName}"]
  }
  }

data "oci_core_vcns" "myvcn" {
  #Required
  compartment_id = "${data.oci_identity_compartments.NetworkComp.compartments.0.id}"
    filter {
    name    = "display_name"
    values  = ["${var.vcn_name}"]
  }
}
  
data "oci_core_subnets" "db_subnet" {
  #Required
  compartment_id = "${data.oci_identity_compartments.NetworkComp.compartments.0.id}"
  vcn_id         = "${data.oci_core_vcns.myvcn.virtual_networks.0.id}"
  filter {
    name   = "display_name"
    values = ["${var.db_subnet_name}"]
  }
}

data "oci_core_subnets" "db_backup_subnet" {
  #Required
  compartment_id = "${data.oci_identity_compartments.NetworkComp.compartments.0.id}"
  vcn_id         = "${data.oci_core_vcns.myvcn.virtual_networks.0.id}"
  filter {
    name   = "display_name"
    values = ["${var.db_backup_subnet_name}"]
  }
}