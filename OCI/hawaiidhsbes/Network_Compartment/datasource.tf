
data "oci_identity_compartments" "mycomp" {
  compartment_id = "${var.tenancy_ocid}"
  filter {
    name   = "name"
    values = ["${var.compartment_name}"]
  }
  }

data "oci_core_route_tables" "my_default_route_table" {
    #Required
    compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
    vcn_id = "${oci_core_virtual_network.vnc.id}"
    filter {
    name = "display_name"
    values = ["^Default"]
    regex = true
    }
}

data "oci_core_cpes" "cpes" {
    compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
}

data "oci_core_ipsec_connections" "ip_sec_connections" {
    #Required
    compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
}

data "oci_core_ipsec_config" "ip_sec_connection_device_config" {
    #Required
    ipsec_id = "${data.oci_core_ipsec_connections.ip_sec_connections.connections.0.id}"
}
