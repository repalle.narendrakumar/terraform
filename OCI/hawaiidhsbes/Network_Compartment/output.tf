/*
# Output the result
output "show-mycompid" {
  value = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
}

#Output the result
output "show_myvcn_names" {
  value = "${data.oci_core_vcns.myvcn.virtual_networks.0.display_name}"
}

#Output the result
output "show_myvcn_id" {
  value = "${data.oci_core_vcns.myvcn.virtual_networks.0.id}"
}

# Output the result
output "show-mysubnet_names" {
  value = "${data.oci_core_subnets.mysubnets.*.subnets}"
}

# Output the result
output "show-mysubnet_id" {
  value = "${data.oci_core_subnets.mysubnets.*.subnets}"
}

# Output the result
output "show-cpes" {
  value = "${data.oci_core_cpes.cpes.cpes}"
}

# Output the result
output "show-IPsecs" {
  value = "${data.oci_core_ipsec_connections.ip_sec_connections.connections}"
}
*/

# Output the result
output "show_IPsec_connection_config_details" {
  value = "${data.oci_core_ipsec_config.ip_sec_connection_device_config.tunnels}"
}