
resource "oci_core_cpe" "cpe" {
    #Required
    count           = "${length(var.cpe_info)}"
    compartment_id  = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
    display_name    = "${element(split(",",var.cpe_info[count.index]),0)}_cpe"
    ip_address      = "${element(split(",",var.cpe_info[count.index]),1)}"
}

resource "oci_core_drg" "drg" {
    #Required
    compartment_id   = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
    display_name     = "${var.dynamic_routing_gateway_name}"
}

resource "oci_core_drg_attachment" "drg_attachment" {
    drg_id = "${oci_core_drg.drg.id}"
    vcn_id = "${oci_core_virtual_network.vnc.id}"
    display_name =    "${var.dynamic_routing_gateway_name}_attach"
}

resource "oci_core_ipsec" "ip_sec_connection" {
    count          = "${length(data.oci_core_cpes.cpes.cpes)}"
    compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
    cpe_id         = "${lookup(data.oci_core_cpes.cpes.cpes[count.index],"id")}"
    drg_id         = "${oci_core_drg.drg.id}"
    static_routes  = ["0.0.0.0/0"]
    display_name   = "${element(split(",",var.cpe_info[count.index]),0)}_IPsec"
}
