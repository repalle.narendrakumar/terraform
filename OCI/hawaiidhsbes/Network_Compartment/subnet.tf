// Public subnet
resource "oci_core_subnet" "public_subnet" {
  count             = "${length(var.hawaii_public_subnets)}"
  display_name      = "${element(split(",",var.hawaii_public_subnets[count.index]),0)}"
  dns_label         = "pubsub${count.index + 1}"
  cidr_block        = "${element(split(",",var.hawaii_public_subnets[count.index]),1)}"
  compartment_id    = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
  vcn_id            = "${oci_core_virtual_network.vnc.id}"
  security_list_ids = ["${oci_core_security_list.Public_Security_Lists.id}"]
  route_table_id    = "${oci_core_route_table.route_table_internet_gateway.id}"
  dhcp_options_id   = "${oci_core_dhcp_options.dhcp_options.id}"
}

// Private subnet for internal DHCP
resource "oci_core_subnet" "private_subnet" {
  count                      = "${length(var.hawaii_private_subnets)}"
  display_name               = "${element(split(",",var.hawaii_private_subnets[count.index]),0)}"
  dns_label                  = "prisub${count.index + 1}"
  cidr_block                 = "${element(split(",",var.hawaii_private_subnets[count.index]),1)}"
  compartment_id             = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
  vcn_id                     = "${oci_core_virtual_network.vnc.id}"
  security_list_ids          = ["${oci_core_security_list.Private_Security_Lists.id}"]
  route_table_id             = "${oci_core_route_table.Private_subnet_Route_Table.id}"
  prohibit_public_ip_on_vnic = "true"
  dhcp_options_id = "${element(split(",",var.hawaii_private_subnets[count.index]),2) == "internal_dns" ? oci_core_dhcp_options.dhcp_options.id : oci_core_virtual_network.vnc.default_dhcp_options_id}"
}

