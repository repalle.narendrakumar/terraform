resource "oci_core_security_list" "Public_Security_Lists" {
  compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
  vcn_id         = "${oci_core_virtual_network.vnc.id}"
  display_name   = "${var.Public_sec_list_name}"

              ingress_security_rules {
              protocol   = "all"
              source    = "10.40.0.0/16"
            }
              ingress_security_rules {
              protocol   = "all"
              source    = "10.20.0.0/16"
            } 
              ingress_security_rules {
              protocol   = "all"
              source    = "10.19.0.0/16"
            } 
              ingress_security_rules {
              protocol   = "all"
              source    = "192.168.0.0/16"
            } 
################Rule For OpenVPN#######################
              ingress_security_rules {
              protocol  = "17"         // udp
              source    = "0.0.0.0/0"
                        udp_options {
                            "min" = 2525
                            "max" = 2525
                          }
                  }
########################################################        
              egress_security_rules {
              protocol       = "all"
              destination    = "0.0.0.0/0"   
            }
}

resource "oci_core_security_list" "Private_Security_Lists" {
  compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
  vcn_id         = "${oci_core_virtual_network.vnc.id}"
  display_name   = "${var.Privates_sec_list_name}"
  
                  ingress_security_rules {
                  protocol  = "all"
                  source    = "${var.security_list_ingress_security_rules_destination}"
                }

                  egress_security_rules {
                  protocol       = "all"
                  destination    = "0.0.0.0/0"   
                  }
}
