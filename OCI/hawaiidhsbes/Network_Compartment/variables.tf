//compartment_name
variable "compartment_name" {
  default = "Network_Compartment"
}


variable "vcn_name" {
  default = "Dev_VCN"
}

variable "dns_label" {
  default = "hawaii"
}
variable "cidr_block_vnc" {
  default = "10.40.0.0/16"
}

variable "hawaii_public_subnets" {
  default = {
    "0" = "Management_Public_subnet,10.40.1.0/24"
  }
}

variable "hawaii_private_subnets" {
  default = {
    "0" = "BES_DDI_DEV_Subnet,10.40.3.0/24,internal_dns"
    "1" = "BES_DDI_SH_DEV_Subnet,10.40.4.0/24,internal_dns"
    "2" = "M&O_DEV_Subnet,10.40.5.0/24,internal_dns"
    "3" = "M&O_SH_DEV_Subnet,10.40.6.0/24,internal_dns"
    "4" = "M&O_SIT_Subnet,10.40.7.0/24,internal_dns"
    "5" = "Database_Client_Subnet,10.40.8.0/28,oracle_dns"
    "6" = "Database_Backup_Subnet,10.40.8.16/29,oracle_dns"
    "7" = "BES_DDI_Build_Subnet,10.40.9.0/24,internal_dns"
    "8" = "M&O_Build_Subnet,10.40.10.0/24,internal_dns"
    "9" = "OpenVpnPrivate_Subnet,10.40.2.0/24,internal_dns"
  }
}

variable "IG_Name" {
  default = "Management_InternetGateway"
}
variable "NAT_Name" {
  default = "NAT_Gateway_Private_Subnet"
}
variable "Public_Route_Table" {
  default = "Public_Route_Table"
}
variable "search_domain_names" {
  default = "hawaii.gov"
}
variable "dhcp_name" {
  default = "dev_vcn_Dhcp"
}
variable "custom_dns_servers" {
  default = ["10.40.1.7"]
}
#############Public Seclist##############
variable "Public_sec_list_name" {
  default = "Openvpn_Public_SecurityList"
}

//Ingress Security list destination 
variable "security_list_ingress_security_rules_destination" {
  default = "0.0.0.0/0"
}

variable "pubip" {
  default = ["220.227.26.70/30"]
}

#############Private Seclist##############
variable "Privates_sec_list_name" {
  default = "Private_SecurityList"
}

########Private_IP_Route_Table############
variable "Private_subnet_Route_Table_Name" {
default = "Private_subnet_Route_Table"
}


#######IPSEC VPN##########
variable "cpe_info" {
  default = {
    "0" = "abq,66.193.196.67"
    #"1" = "tvm,121.243.126.134"
  }
}

variable "dynamic_routing_gateway_name" {
  default = "Hawaii_DRG"
}
