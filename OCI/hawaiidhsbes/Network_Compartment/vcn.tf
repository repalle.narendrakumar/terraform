// VCN
resource "oci_core_virtual_network" "vnc" {
  cidr_block     = "${var.cidr_block_vnc}"
  compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
  display_name   = "${var.vcn_name}"
  dns_label      = "${var.dns_label}"
}
