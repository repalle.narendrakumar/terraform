resource "oci_core_internet_gateway" "Internet_gateway" {
  compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
  display_name   = "${var.IG_Name}"
  vcn_id         = "${oci_core_virtual_network.vnc.id}"
}

resource "oci_core_nat_gateway" "NAT_Gateway" {
  compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
  display_name   = "${var.NAT_Name}"
  vcn_id         = "${oci_core_virtual_network.vnc.id}"
}

resource "oci_core_route_table" "route_table_internet_gateway" {
  compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
  display_name   = "${var.Public_Route_Table}"
  vcn_id         = "${oci_core_virtual_network.vnc.id}"
  route_rules {
    network_entity_id = "${oci_core_internet_gateway.Internet_gateway.id}"
    destination       = "0.0.0.0/0"
  }
  route_rules {
    network_entity_id = "${oci_core_drg.drg.id}"
    destination       = "10.20.0.0/16"
  }
  route_rules {
    network_entity_id = "${oci_core_drg.drg.id}"
    destination       = "10.19.0.0/16"
  }
  route_rules {
    network_entity_id = "${oci_core_drg.drg.id}"
    destination       = "192.168.0.0/16"
  }
}
  resource "oci_core_route_table" "Private_subnet_Route_Table" {
  compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
  display_name   = "${var.Private_subnet_Route_Table_Name}"
  vcn_id         = "${oci_core_virtual_network.vnc.id}"
  route_rules {
    network_entity_id = "${oci_core_nat_gateway.NAT_Gateway.id}"
    destination       = "0.0.0.0/0"
  }
  route_rules {
    network_entity_id = "${oci_core_drg.drg.id}"
    destination       = "10.20.0.0/16"
  }
  route_rules {
    network_entity_id = "${oci_core_drg.drg.id}"
    destination       = "10.19.0.0/16"
  }
  route_rules {
    network_entity_id = "${oci_core_drg.drg.id}"
    destination       = "192.168.0.0/16"
  }
  }

resource "oci_core_dhcp_options" "dhcp_options" {
    compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
    options {
        type = "DomainNameServer"
        server_type = "CustomDnsServer"
        custom_dns_servers = "${var.custom_dns_servers}"
    }
    options {
        type = "SearchDomain"
        search_domain_names = ["${var.search_domain_names}"]
    }
    vcn_id = "${oci_core_virtual_network.vnc.id}"
    display_name = "${var.dhcp_name}"
}
