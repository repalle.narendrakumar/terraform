data "oci_identity_availability_domains" "ad" {
  compartment_id = "${var.tenancy_ocid}"
}


data "oci_identity_compartments" "InstanceComp" {
  compartment_id = "${var.tenancy_ocid}"
  filter {
    name   = "name"
    values = ["${var.InstanceCompName}"]
  }
  }

data "oci_identity_compartments" "NetworkComp" {
  compartment_id = "${var.tenancy_ocid}"
  filter {
    name   = "name"
    values = ["${var.NetworkCompName}"]
  }
  }

data "oci_core_vcns" "myvcn" {
  #Required
  compartment_id = "${data.oci_identity_compartments.NetworkComp.compartments.0.id}"
    filter {
    name    = "display_name"
    values  = ["${var.vcn_name}"]
  }
}


data "oci_core_subnets" "BES_DDI_DEV_Subnet" {
  #Required
  compartment_id = "${data.oci_identity_compartments.NetworkComp.compartments.0.id}"
  vcn_id         = "${data.oci_core_vcns.myvcn.virtual_networks.0.id}"
  filter {
    name   = "display_name"
    values = ["${var.BES_DDI_DEV_Subnet_Name}"]
  }
}
data "oci_core_subnets" "BES_DDI_SH_DEV_Subnet" {
  #Required
  compartment_id = "${data.oci_identity_compartments.NetworkComp.compartments.0.id}"
  vcn_id         = "${data.oci_core_vcns.myvcn.virtual_networks.0.id}"
  filter {
    name   = "display_name"
    values = ["${var.BES_DDI_SH_DEV_Subnet_Name}"]
  }
}
data "oci_core_subnets" "BES_DDI_Build_Subnet" {
  #Required
  compartment_id = "${data.oci_identity_compartments.NetworkComp.compartments.0.id}"
  vcn_id         = "${data.oci_core_vcns.myvcn.virtual_networks.0.id}"
  filter {
    name   = "display_name"
    values = ["${var.BES_DDI_Build_Subnet_Name}"]
  }
}

/*
data "oci_core_security_lists" "my_security_list" {
    #Required
    compartment_id = "${data.oci_identity_compartments.mycomp.compartments.0.id}"
    vcn_id = "${data.oci_core_vcns.myvcn.virtual_networks.0.id}"
         filter {
    name   = "display_name"
    values = ["${var.my_sec_list_name}"]
  } 
}
*/