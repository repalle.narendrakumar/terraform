resource "oci_core_instance" "BES_DDI_DEV_Subnet_instances" {
  count               = "${length(var.BES_DDI_DEV_Subnet_instances)}"
  availability_domain = "${data.oci_identity_availability_domains.ad.availability_domains.0.name}"
  compartment_id      = "${data.oci_identity_compartments.InstanceComp.compartments.0.id}"
  display_name        = "${element(split("|",var.BES_DDI_DEV_Subnet_instances[count.index]),0)}"
  shape               = "${element(split("|",var.BES_DDI_DEV_Subnet_instances[count.index]),1)}"
  fault_domain        = "${element(split("|",var.BES_DDI_DEV_Subnet_instances[count.index]),4)}"

  create_vnic_details {
    subnet_id = "${data.oci_core_subnets.BES_DDI_DEV_Subnet.subnets.0.id}"
    assign_public_ip = "${element(split("|",var.BES_DDI_DEV_Subnet_instances[count.index]),3)}"
    display_name = "${element(split("|",var.BES_DDI_DEV_Subnet_instances[count.index]),0)}"
  }

  source_details {
    source_type = "image"
    source_id   = "${element(split("|",var.BES_DDI_DEV_Subnet_instances[count.index]),2)}"
    boot_volume_size_in_gbs = "${element(split("|",var.BES_DDI_DEV_Subnet_instances[count.index]),5)}"
  }

  metadata {
    ssh_authorized_keys = "${file(var.ssh_public_key)}"
    user_data           = "${base64encode(file(var.BootStrapFile))}"
  }
  freeform_tags = {
  OperatingSystemVersion = "${element(split("|",var.BES_DDI_DEV_Subnet_instances[count.index]),6)}"
  VM-Shape               = "${element(split("|",var.BES_DDI_DEV_Subnet_instances[count.index]),1)}"
  HDD                    = "${element(split("|",var.BES_DDI_DEV_Subnet_instances[count.index]),5)}"
  Compartment            = "${var.InstanceCompName}"
  Subnet                 = "${data.oci_core_subnets.BES_DDI_DEV_Subnet.subnets.0.cidr_block}"
  Hostname               =  "${element(split("|",var.BES_DDI_DEV_Subnet_instances[count.index]),0)}.dhsie.hawaii.gov"
  }
}



resource "oci_core_instance" "BES_DDI_SH_DEV_Subnet_instances" {
  count               = "${length(var.BES_DDI_SH_DEV_Subnet_instances)}"
  availability_domain = "${data.oci_identity_availability_domains.ad.availability_domains.0.name}"
  compartment_id      = "${data.oci_identity_compartments.InstanceComp.compartments.0.id}"
  display_name        = "${element(split("|",var.BES_DDI_SH_DEV_Subnet_instances[count.index]),0)}"
  shape               = "${element(split("|",var.BES_DDI_SH_DEV_Subnet_instances[count.index]),1)}"
  fault_domain        = "${element(split("|",var.BES_DDI_SH_DEV_Subnet_instances[count.index]),4)}"

  create_vnic_details {
    subnet_id = "${data.oci_core_subnets.BES_DDI_SH_DEV_Subnet.subnets.0.id}"
    assign_public_ip = "${element(split("|",var.BES_DDI_SH_DEV_Subnet_instances[count.index]),3)}"
    display_name = "${element(split("|",var.BES_DDI_SH_DEV_Subnet_instances[count.index]),0)}"
  }

  source_details {
    source_type = "image"
    source_id   = "${element(split("|",var.BES_DDI_SH_DEV_Subnet_instances[count.index]),2)}"
    boot_volume_size_in_gbs = "${element(split("|",var.BES_DDI_SH_DEV_Subnet_instances[count.index]),5)}"
  }

  metadata {
    ssh_authorized_keys = "${file(var.ssh_public_key)}"
    user_data           = "${base64encode(file(var.BootStrapFile))}"
  }
  freeform_tags = {
  OperatingSystemVersion = "${element(split("|",var.BES_DDI_SH_DEV_Subnet_instances[count.index]),6)}"
  VM-Shape               = "${element(split("|",var.BES_DDI_SH_DEV_Subnet_instances[count.index]),1)}"
  HDD                    = "${element(split("|",var.BES_DDI_SH_DEV_Subnet_instances[count.index]),5)}"
  Compartment            = "${var.InstanceCompName}"
  Subnet                 = "${data.oci_core_subnets.BES_DDI_SH_DEV_Subnet.subnets.0.cidr_block}"
  Hostname               =  "${element(split("|",var.BES_DDI_SH_DEV_Subnet_instances[count.index]),0)}.dhsie.hawaii.gov"
  }
}

resource "oci_core_instance" "BES_DDI_Build_Subnet_instances" {
  count               = "${length(var.BES_DDI_Build_Subnet_instances)}"
  availability_domain = "${data.oci_identity_availability_domains.ad.availability_domains.0.name}"
  compartment_id      = "${data.oci_identity_compartments.InstanceComp.compartments.0.id}"
  display_name        = "${element(split("|",var.BES_DDI_Build_Subnet_instances[count.index]),0)}"
  shape               = "${element(split("|",var.BES_DDI_Build_Subnet_instances[count.index]),1)}"
  fault_domain        = "${element(split("|",var.BES_DDI_Build_Subnet_instances[count.index]),4)}"

  create_vnic_details {
    subnet_id = "${data.oci_core_subnets.BES_DDI_Build_Subnet.subnets.0.id}"
    assign_public_ip = "${element(split("|",var.BES_DDI_Build_Subnet_instances[count.index]),3)}"
    display_name = "${element(split("|",var.BES_DDI_Build_Subnet_instances[count.index]),0)}"
  }

  source_details {
    source_type = "image"
    source_id   = "${element(split("|",var.BES_DDI_Build_Subnet_instances[count.index]),2)}"
    boot_volume_size_in_gbs = "${element(split("|",var.BES_DDI_Build_Subnet_instances[count.index]),5)}"
  }

  metadata {
    ssh_authorized_keys = "${file(var.ssh_public_key)}"
    user_data           = "${base64encode(file(var.BootStrapFile))}"
  }
  freeform_tags = {
  OperatingSystemVersion = "${element(split("|",var.BES_DDI_Build_Subnet_instances[count.index]),6)}"
  VM-Shape               = "${element(split("|",var.BES_DDI_Build_Subnet_instances[count.index]),1)}"
  HDD                    = "${element(split("|",var.BES_DDI_Build_Subnet_instances[count.index]),5)}"
  Compartment            = "${var.InstanceCompName}"
  Subnet                 = "${data.oci_core_subnets.BES_DDI_Build_Subnet.subnets.0.cidr_block}"
  Hostname               =  "${element(split("|",var.BES_DDI_Build_Subnet_instances[count.index]),0)}.dhsie.hawaii.gov"
  }
}