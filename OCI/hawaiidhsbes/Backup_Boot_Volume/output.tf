/*
output "List_boot_volume_backups" {
	value = "${data.oci_core_boot_volume_backups.boot_volume_backups.boot_volume_backups}"
}

output "List_boot_volumes" {
	value = "${data.oci_core_boot_volumes.boot_volumes.boot_volumes}"
}
*/
output "Backup_policies" {
	value = "${data.oci_core_volume_backup_policies.volume_backup_policies.volume_backup_policies.0.id}"
}