# Defines compartment name for instance
variable "InstanceCompName" {
  default = "BES_DDI_Compartment"
}

variable "bootvolumes" {
    type = "list"
	default = ["dev21cvprt01 (Boot Volume)", "dev91cvopa01 (Boot Volume)", "dev91cvsoa01 (Boot Volume)", "dev21cvsoa01 (Boot Volume)", "dev91cvoag01 (Boot Volume)", "dev91cvobi01 (Boot Volume)"]
}

variable "backup_type" {
  type = "map"

  default = {
    "dev21cvprt01 (Boot Volume)" = "FULL"
    "dev91cvopa01 (Boot Volume)" = "FULL"
    "dev91cvsoa01 (Boot Volume)" = "FULL"
    "dev21cvsoa01 (Boot Volume)" = "FULL"
    "dev91cvoag01 (Boot Volume)" = "FULL"
    "dev91cvobi01 (Boot Volume)" = "FULL"
  }
}

variable "backup_policy" {
default = "silver"
}