data "oci_identity_availability_domains" "ad" {
  compartment_id = "${var.tenancy_ocid}"
}

data "oci_identity_compartments" "InstanceComp" {
  compartment_id = "${var.tenancy_ocid}"
  filter {
    name   = "name"
    values = ["${var.InstanceCompName}"]
  }
  }

data "oci_core_boot_volume_backups" "boot_volume_backups" {
    #Required
    compartment_id = "${data.oci_identity_compartments.InstanceComp.compartments.0.id}"
    }

data "oci_core_boot_volumes" "boot_volumes" {
    #Required
    availability_domain = "${data.oci_identity_availability_domains.ad.availability_domains.0.name}"
    compartment_id = "${data.oci_identity_compartments.InstanceComp.compartments.0.id}"
        filter {
        name = "display_name"
        values = "${var.bootvolumes}"
        }
}

data "oci_core_volume_backup_policies" "volume_backup_policies" {
  filter {
    name   = "display_name"
    values = ["${var.backup_policy}"]
  }
}