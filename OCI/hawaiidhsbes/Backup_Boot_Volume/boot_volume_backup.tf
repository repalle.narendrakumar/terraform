resource "oci_core_boot_volume_backup" "boot_volume_backup" {
    #Required
    count = "${length(data.oci_core_boot_volumes.boot_volumes.boot_volumes)}"
    boot_volume_id = "${lookup(data.oci_core_boot_volumes.boot_volumes.boot_volumes[count.index],"id")}"

    #Optional
    display_name = "${lookup(data.oci_core_boot_volumes.boot_volumes.boot_volumes[count.index],"display_name")}"
    type = "${lookup(var.backup_type, lookup(data.oci_core_boot_volumes.boot_volumes.boot_volumes[count.index],"display_name"))}"
}

resource "oci_core_volume_backup_policy_assignment" "volume_backup_policy_assignment" {
    #Required
	count = "${length(data.oci_core_boot_volumes.boot_volumes.boot_volumes)}"
    asset_id = "${lookup(data.oci_core_boot_volumes.boot_volumes.boot_volumes[count.index],"id")}"
    policy_id = "${data.oci_core_volume_backup_policies.volume_backup_policies.volume_backup_policies.0.id}"
}